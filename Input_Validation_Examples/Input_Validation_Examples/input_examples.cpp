#include <iostream>
#include <string>
#include "validate_input.h" // the file showing examples 

int main() {
	
	// some variables of different basic data types, they MUST be declared so there is memory avalible for function call to fill
	int x = 0;
	float y = 0;
	double z = 0;
	std::string s ="";
	
	// now call function by passing the variable you want to hold the data as the first parm, cout statement as a second
	// If you declare a integer, and the user puts in a float, it will be truncated to fit data type the variable will hold if		
	getInputFromUser(x, "Please enter and integer value: ");	
	getInputFromUser(y, "Please enter a float value: ");
	getInputFromUser(z, "Please enter a double value: ");

	// one that will get a string from a user up to the end of the new line and plays nice with the other function
	getLineFromUser(s, "Please enter a string: ");
	
	// show values that were entered by user
	std::cout << "\nThe values entered were: " << x << "\t" << y << "\t" << z << "\t" << s << "\n" << std::endl;
	
	//************************* Values with a min requirement **********************************************
	
	// if you want a minimum value, unless you are using ints, you must declare a variable that holds a max value
	// they can be constants or not so long as they exist
	float const flt_MIN_VALUE = 0.0;
	double dbl_MIN_VALUE = -65.05; // can use neg #'s too

	// call the same way as before now just add a 3rd parameter that holds your minimum value desired
	getInputFromUser(x, "Please enter and integer value: ", 0);	
	getInputFromUser(y, "Please enter a float value: ", flt_MIN_VALUE);
	getInputFromUser(z, "Please enter a double value: ", dbl_MIN_VALUE);

	// show values entered by user
	std::cout << "\nThe values with minimum requirement entered were: " << x << "\t" << y << "\t" << z << "\t" << s << "\n" << std::endl;

		//************************* Values with a min and max requirement **********************************************

	// if you want min and max values, same rule for max, must be a declared value unless you use and integer then you can just
	// declare max values, again can be constants or not
	float flt_MAX_VALUE = 105.345;
	double const dbl_MAX_VALUE = 25.5634;
	
	// call the same way as before now just add a 4th parameter that holds your max value desired
	getInputFromUser(x, "Please enter and integer value: ", 0, 100);	
	getInputFromUser(y, "Please enter a float value: ", flt_MIN_VALUE, flt_MAX_VALUE);
	getInputFromUser(z, "Please enter a double value: ", dbl_MIN_VALUE, dbl_MAX_VALUE);

	//// show values entered by user
	std::cout << "\nThe values with min, and max requirements entered were: " << x << "\t" << y << "\t" << z << "\t" << s << "\n" << std::endl;


	//************************* Values with a min and max requirement and custom error message **********************************************
	// this one works great for a menu type situation, where you want to have a custom error message
	// such as to remind the user of avalible choices to choose from
	// converting the values to string so only updating min and max value variable will be updated automatically, but not nessessary.
	// call the same way as before now just add a 5th parm of a sting for the cout statement if error occurs while entering value
	getInputFromUser(x, "Please enter and integer value: ", 0, 100, "\n\nError\n\tPlease enter a integer value between 1-100\n\n");	
	getInputFromUser(y, "Please enter a float value: ", flt_MIN_VALUE, flt_MAX_VALUE, "\n\nError\n\tPlease enter a value between " +
		std::to_string(flt_MIN_VALUE) + " and " + std::to_string(flt_MAX_VALUE) + "\n"); 
	getInputFromUser(z, "Please enter a double value: ", dbl_MIN_VALUE, dbl_MAX_VALUE, "\n\nError:\n\tPlease enter a value between " +
		std::to_string(dbl_MIN_VALUE) + " and " + std::to_string(dbl_MAX_VALUE) + "\n");
	// show values entered by user
	std::cout << "\nThe values with min, max and custom error entered were: " << x << "\t" << y << "\t" << z << "\t" << s << "\n" << std::endl;
	
	std::getchar(); // pause and wait for usr to click to exit
}