#include <iostream>
#include <string>

#ifndef VALIDATION_H
#define VALIDATION_H

/*============================== Mikes_Validation_Header.h ===========================
My template created for input validation on input from user
Includes a overloaded function that takes 2-5 arguments.
Optional Arguments 3 & 4 (range) must be same data type as 1st. (5) is optional error msg

Author: Michael Lewis
Version: 3.2
Date: June 14, 2015
====================================================================================*/

// Constant Strings used in template
std::string const str_ERROR = "\n\nERROR: ";
std::string const str_INVALID = "\n\tInvalid data type entered\n\n";
std::string const str_MIN_REQ = "\n\tValue does not meet minimum requirement\n\n";
std::string const str_MAX_REQ = "\n\tValue exceeds maximum value allowed\n\n";

void clrStrm(){
    std::cin.clear();
    std::cin.ignore( 10000, '\n' );
}

/*============================ getInputFromUser TEMPLATE =================================
Function validate data type from the user, returns valid data back to requesting procedure
Pre-Conditions: output var, a string to pass to prompt user for input
Post Condition: Output variable will hold a valid value, based on declared data type
Parameters:		(output variable, prompt)
==========================================================================================*/
template <class TYPE1>
void getInputFromUser( TYPE1 &input, const std::string coutStatement ){
	while(1) {
        std::cout << coutStatement;
	    if (std::cin >> input){
            clrStrm();
            return;
	    } // END if
        else {
            clrStrm();
            std::cout << str_ERROR << str_INVALID;
        } // END if
	} // END while
} // END getInputFromUser()

/*============================ getInputFromUser TEMPLATE =================================
Overloaded Function validates data type from the user with a specified minimum value of same type
Pre-Conditions: data type that allows relational operations, a string for prompt
Post Condition: Output variable will hold a valid value, on declared data type with minimum enforced
Parameters:		(output variable, prompt, min value)
==========================================================================================*/
template <class TYPE1>
void getInputFromUser(	TYPE1 &input,
						const std::string coutStatement,
						const TYPE1 minRange ){
	while(1)	{
		std::cout << coutStatement;
		if (std::cin >> input) {
		    clrStrm();
            if ( input < minRange )
                std::cout << str_ERROR << str_MIN_REQ;
            else
                return;
		} // END if
		else {
            clrStrm();
            std::cout << str_ERROR << str_INVALID;
		} // END else
	} // END while
} // END getInputFromUser()

/*============================ getInputFromUser TEMPLATE =================================
Overloaded Function validate data type from the user with a specified minimum value of same type
Pre Conditions: data type that allows relational operations, a string for cout prompt, min & max of same data type
Post Condition: Output variable will hold a valid value, on declared data type within specified range
Parameters:		(output variable, prompt, min value, max value)
==========================================================================================*/
template <class TYPE1>
void getInputFromUser(  TYPE1 &input,
						const std::string coutStatement,
						const TYPE1 minRange,
						const TYPE1 maxRange ){
	while(1) {
	    std::cout << coutStatement;
		if (std::cin >> input){
            clrStrm();
		    if ( input < minRange )
		        std::cout << str_ERROR << str_MIN_REQ;
			else if ( input > maxRange )
            		std::cout << str_ERROR << str_MAX_REQ;
			else
                return;
		} // END if
        else {
            clrStrm();
            std::cout << str_ERROR << str_INVALID;
        } // END else
	} // END while
} // END getInputFromUser()

/*============================ getInputFromUser TEMPLATE =================================
Overloaded Function validate data type from the user with a specified minimum value
				of same type with customized error message
Pre Conditions: data type that allows relational operations, a string for cout prompt,
				min & max of same data type, string for out of range msg
Post Condition: Output variable will hold a valid value, on declared data type within specified range
Parameters:		(output variable, prompt string, min value, max value, error msg)
==========================================================================================*/
template <class TYPE1>
void getInputFromUser(	TYPE1 &input,
						const std::string coutStatement,
						const TYPE1 minRange,
						const TYPE1 maxRange,
						const std::string errorMessage ){
	while(1)	{
        std::cout << coutStatement;
        if ( (std::cin >> input) && (input >= minRange) && (input <= maxRange) ) {
            clrStrm();
            return;
        } // END if
        else {
            clrStrm();
            std::cout << errorMessage;
        } // END else
	} // END while
} // END getInputFromUser()

/*============================ getLineFromUser TEMPLATE =================================
Function uses getline to retrieve data from user (string format only)
Pre Conditions: output var, a string to pass to prompt user for input
Post Condition: Output variable will hold a valid value, based on declared data type
Parameters:		(output variable, prompt)
==========================================================================================*/
template <class TYPE1>
void getLineFromUser( TYPE1 &input, const std::string coutStatement )
{
	while(1)	{
        std::cout << coutStatement;
        if(std::getline( std::cin, input ))
            return;
        else {
            clrStrm();
            std::cout << str_ERROR << str_INVALID;
        } // END else
    } // END while
} // END getInputFromUser()

#endif // END VALIDATION_H
